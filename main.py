import parseData
import prim
import scatter
import time
import json
from collections import defaultdict
from ordered_set import OrderedSet

# initial distance 
distance = 0
"""
	Return a candidate path that is a short path in the current grapg
	@param	root	the start node (e.g. node with the index 0)
	@param 	allNodes	an array that represent all available nodes in graph
	@param 	graph	the graph represented like a dictionary
	{
		node: [(nextNodeNo, cost)]; e.g. 0: [(1, 12.43), (2,54.123)]
	}
	@param visitedNodes	an array with all nodes already visieted, by default it's a empty array
	@param path	an array with all nodes visited - some nodes can be dublicated 
	@param fallback keep algorithm tracking in order to return to a parent if a leaf is reached
"""
def getPath(root, allNodes, graph, visitedNodes = [], path = [], fallback = []):
	global distance
	
	while len(visitedNodes) != len(allNodes):
		path.append(root)

		if root not in visitedNodes:
			visitedNodes.append(root)

		item = graph.get(root)
		
		if root not in fallback or root != fallback[len(fallback) - 1]:
			fallback.append(root) 
		
		if item:
			for idx, (nextNode, cost) in enumerate(item):
				if nextNode not in visitedNodes:
					distance = distance + cost
					getPath(nextNode, allNodes, graph, visitedNodes, path, fallback)
				else:
					# if the current node has children 
					if root in fallback and nextNode in fallback and nextNode in visitedNodes and len(item) > 1 and idx < len(item) - 1:
						continue
					else:
						distance = distance + cost
						idx = fallback.index(root)
						getPath(fallback[idx - 1], allNodes, graph, visitedNodes, path, fallback)
		else:
			# should go back to parents
			idx = fallback.index(root)
			
			# add distance fallback
			fallbackNode = fallback[idx - 1]
			parent = graph[fallbackNode]
			for (child, cost) in parent:
				if child == root:
					distance = distance + cost

			getPath(fallback[idx - 1], allNodes, graph, visitedNodes, path, fallback)
	return path

"""
	Return a dictionary with all direct connected nodes sorted by cost
	@param MST	a list of all nodes connections
	'' MST: [
		(nodeX, connectedNodeWithX = Y, distanceXY)
		(nodeX, connectedNodeWithX = Z, distanceXZ)
		...
	]
	''
	@return a dictionary with connected nodes sorted by distance
	'' {
		nodeX: 	[(Y, distanceXY), (Z, distanceXZ)]
		...
	}
	''
"""
def groupNodes(MST):
	mst = defaultdict(list)
	for frm, to, cost in MST:
		mst[frm].append((to, cost))

	for el in mst:
		lst = mst[el]
		# sort ascending by nodes distance
		lst.sort(key=lambda x:x[1])
		mst[el] = OrderedSet(lst).items

	return dict(mst)

def start():
	# start execution time
	startTime = time.time()

	# start node
	startNode = 0

	# read data from the file
	with open('input.txt', 'r') as f:
		points = json.load(f)

	# add Euclidian distance for all nodes
	parsedData = parseData.transformData(points)

	# perform Minimum Spanning Tree
	MST = prim.create_spanning_tree(parsedData, startNode)

	# add all edges for graph
	edges = [
        (frm, to)
        for frm, to, cost in MST
    ]

	# create a list with all nodes
	allNodes = list(range(len(points)))

	# create a dictionary sorted by cost
	graph = groupNodes(MST)

	# minimum path
	path = getPath(startNode, allNodes, graph)

	# end execution time
	endTime = time.time()
	totalTime = str(endTime - startTime)

	print('Path ', path)
	print('Distance ', distance)
	print('Time ', totalTime, 'seconds')

	# draw graph
	scatter.draw(points, edges)

if __name__ == "__main__":
	start()