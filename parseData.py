from scipy.spatial import distance

"""
    Add distance between all nodes
    @param    data    bi-dimensional array [[23, 141], [428, 3]]
    @return  a set of nodes
    ''
        {
            node0: {
                node1: 12.323
                node2: 10.212
                ...    
            },
            node1: {}
        }
    ''
"""
def transformData(data):
    trans = {}
    for idxCurrent, current in enumerate(data):
        trans[idxCurrent] = {}
        for idxTarget, target in enumerate(data):
            if idxCurrent != idxTarget:
                # add euclidean distance 
                dist = distance.euclidean(current, target)
                trans[idxCurrent].update({
                    idxTarget: dist
                })
    return trans

