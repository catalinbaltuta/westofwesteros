from collections import defaultdict
import heapq


"""
    @param  graph   a set of nodes with the distance between them
    ''
        {
            node0: {
                node1: 12.323
                node2: 10.212
                ...    
            },
            node1: {}
        }
    ''
    @param  starting_vertex the start node
    @return a list with shortest edges that cover all nodes
"""
def create_spanning_tree(graph, starting_vertex):
    mst = []
    visited = set([starting_vertex])
    edges = [
        (cost, starting_vertex, to)
        for to, cost in graph[starting_vertex].items()
    ]
    heapq.heapify(edges)

    while edges:
        cost, frm, to = heapq.heappop(edges)
        if to not in visited:
            visited.add(to)
            mst.append((frm, to, cost))
            
            for to_next, cost in graph[to].items():
                if to_next not in visited:
                    heapq.heappush(edges, (cost, to, to_next))
    return mst