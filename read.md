# West of Westeros

Given a bi-dimensional JSON array with coordinates (e.g. [[23, 141], [428, 3]]) that represents a list of 
coordinates into the cartesian space. The algorithm will return a path that can be walked between all locations in an efficient mode ( in order to minimize the distance )

**The input data is read from the input.txt file**


#### Getting Started
    1. git clone https://catalinbaltuta@bitbucket.org/catalinbaltuta/westofwesteros.git
    2. cd westofwesteros
    2. git checkout master
    3. git pull origin master

    4. python main.py 
        or
       py main.py

### Prerequisites
    python 3.6 instaled
	order-set lib (pip install ordered-set)
    # draw graph
    python -m pip install numpy scipy matplotlib
   

### Details
    To perform all connection between any two locations we used Prim's Minimum Spanning Tree (MST) algorithm. Because in the initial phase we can suppose that all location can be connected, that means a huge number of edges. In order to minimize the time we use a Priority Queue.

    When the best edges were created we using DSF ( greedy ) to try to find the minimum path. The decision which node it's the next one is made based on cost ( cost = distance between two nodes). 
    If the node **i** was visited into the step s in the next iteration will be selected the node with the minimum distance from **i** . This 'naive' approach was made to minimize the distance for fallback case - we cannot move forward and should return through already visited nodes.

### Notes
    In order to simplify the graph, we counted the nodes ( e.g. start node with the coordinates [x,y] became 0). To see the coordinates of the node we can hover over it with the cursor


### Resources

* https://gist.github.com/siddMahen/8261350
* https://bradfieldcs.com/algos/graphs/prims-spanning-tree-algorithm/
* https://stackoverflow.com/questions/21822592/plot-a-scatter-plot-in-python-with-matplotlib-with-dictionary
