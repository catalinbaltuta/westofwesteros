import numpy
import matplotlib.pyplot as plt

"""
    Draw a graph into the cartesian space
    @param  nodes   all nodes from the graph
    @param  edges   all edges from the graph
"""
def draw(nodes, edges):
    offset = 5

    points = numpy.array(nodes)
    edges = numpy.array(edges)

    x = points[:,0].flatten()
    y = points[:,1].flatten()
    
    plt.plot(
        x[edges.T], 
        y[edges.T], 
        linestyle='-', 
        color='b',
        markerfacecolor='red', 
        marker='o'
    ) 

    #  add labels
    for node1, node2 in edges:
        lblNode1 = points[node1]
        lblNode2 = points[node2]
        
        plt.annotate(str(node1), (lblNode1[0] + offset, lblNode1[1] - offset))
        plt.annotate(str(node2), (lblNode2[0] + offset, lblNode2[1] - offset))

    plt.title('West Of Westeros')
    plt.show()
